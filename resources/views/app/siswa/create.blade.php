<!-- Penanda template yang digunakan adalah dari app.blade.php -->
@extends('app._layouts.app')

@section('content')

<div class="card m-4">
    <div class="card-header bg-primary text-white">
        Tambah Data Siswa
    </div>
    <div class="card-body">

        @if($errors->any())

            <div class="alert alert-danger d-flex align-items-center" role="alert" style="max-height: 30px;">
                <svg class="bi flex-shrink-0 me-2" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                <div>
                    Terdapat Kendala Dalam Memasukkan Data Anda
                </div>
            </div>
        @endif

        <form action="{{ route('siswa.store') }}" method="POST" enctype="multipart/form-data">
            {{-- Kebutuhan keamanan, pengiriman token --}}
            @csrf

            {{-- Untuk Form Pengisian Nama Lengkap --}}
            <div class="mb-3">
                <label for="nama" class="form-label">
                    Nama Lengkap<span class="text-danger">*</span>
                </label>
                <input type="text" name="nama" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" 
                    id="nama" placeholder="Nama Lengkap.."
                    value="{{ old('name') }}" required
                    >
                @error('nama')
                    <span class="text-danger mt-1" style="font-size: 12px;">
                        {{ $message }}
                    </span>
                @enderror
            </div>

            {{-- Untuk Form Pengisian Nomor Induk --}}
            <div class="mb-3">
                <label for="nomor_induk" class="form-label">
                    Nomor Induk<span class="text-danger">*</span>
                </label>
                <input type="text" name="nomor_induk" class="form-control {{ $errors->has('nomor_induk') ? 'is-invalid' : '' }}" 
                    id="nomor_induk" placeholder="Nomor Induk.."
                    value="{{ old('nomor_induk') }}" required
                    >
                @error('nomor_induk')
                    <span class="text-danger mt-1" style="font-size: 12px;">
                        {{ $message }}
                    </span>
                @enderror
            </div>

            {{-- Untuk Form Pengisian Program Studi --}}
            <div class="mb-3">
                <label for="prodi_id" class="form-label">
                    Program Studi<span class="text-danger">*</span>
                </label>
                <select class="form-select {{ $errors->has('prodi_id') ? 'is-invalid' : '' }}" id="prodi_id" name="prodi_id" required>
                    <option value="">-- Pilih Program Studi --</option>
                    @foreach($prodis as $prodi)
                        <option value="{{ $prodi['id'] }}" {{ $prodi['id'] == old('prodi_id') ? 'selected' : '' }}>
                            {{ $prodi['nama'] }}
                        </option>
                    @endforeach
                </select>
                @error('prodi_id')
                    <span class="text-danger mt-1" style="font-size: 12px;">
                        {{ $message }}
                    </span>
                @enderror
            </div>

            <div class="mb-3">
                <label for="image" class="form-label">
                    Image<span class="text-danger">*</span>
                </label>
                <input class="form-control form-control-sm {{ $errors->has('is-invalid') ? 'is-invalid' : '' }}" 
                    id="image" type="file" required name="image"
                    accept="image/png, image/jpeg, image/jpg">
                @error('image')
                    <span class="text-danger mt-1" style="font-size: 12px;">
                        {{ $message }}
                    </span>
                @enderror
            </div>

            <button type="submit" class="btn btn-sm btn-primary">
                <i class="fa fa-save"></i> Simpan
            </button>

            {{-- Untuk Pengisian Gambar --}}
        </form>
    </div>
</div>

@endsection