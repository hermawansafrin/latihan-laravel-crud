<!-- Penanda template yang digunakan adalah dari app.blade.php -->
@extends('app._layouts.app')

@section('content')

<div class="card m-4">
    <div class="card-header bg-primary text-white">
        Data Siswa
    </div>
    <div class="card-body">

        <a class="btn btn-sm btn-primary" href="{{ route('siswa.create') }}">
            <i class="fa fa-plus"></i> Tambah Data
        </a>

        @if(session('status'))
            <p class="text-success">
                {{ session('status') }}
            </p>
        @endif

        <table class="table table-hover table-responsive table-bordered mt-2">
            <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Nama Lengkap</th>
                    <th class="text-center">Nomor Induk</th>
                    <th class="text-center">Program Studi</th>
                    <th class="text-center">Foto</th>
                    <th class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                {{-- Mencatatkan seluruh data siswa yang didapatkan --}}
                @forelse($datas as $data)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td class="text-center">{{ $data['nama'] }}</td>
                        <td class="text-center">{{ $data['nomor_induk'] }}</td>
                        <td class="text-center">{{ $data['prodi']['nama'] ?? '-' }}</td>
                        <td class="text-center">
                            <img class="img img-responsive" style="max-height:60px;" src="{{ asset('storage/images/'.$data['image']) }}">
                        </td>
                        <td class="text-center">
                            {{-- Button Edit --}}
                            <div>
                                <a class="btn btn-sm btn-warning m-1" href="{{ route('siswa.edit', ['id' => $data['id']]) }}">
                                    <i class="fa fa-pencil"></i> Edit
                                </a>
                            </div>

                            {{-- Button Delete --}}
                            <div>
                                <form method="POST" onsubmit="if(!confirm('Apakah anda yakin menghapus data ini ?')){return false;}" 
                                        action="{{ route('siswa.destroy', $data['id']) }}"
                                    >
                                    @csrf
                                    @method('DELETE')

                                    <button class="btn btn-sm btn-danger m-1" type="submit">
                                        <i class="fa fa-trash"></i> Hapus
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                {{-- Skenario jika kosong --}}
                @empty
                    <tr>
                        <td colspan="6" class="text-center">Data Kosong</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

@endsection