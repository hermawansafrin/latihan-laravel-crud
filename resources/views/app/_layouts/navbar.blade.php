<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid">
        <a class="navbar-brand navbar-white" href="{{ route('index') }}">Home</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse text-white" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link {{ request()->routeIs('siswa.index') ? 'active' : '' }}" aria-current="page" href="{{ route('siswa.index') }}">Data Siswa</a>
            </div>
        </div>
    </div>
</nav>