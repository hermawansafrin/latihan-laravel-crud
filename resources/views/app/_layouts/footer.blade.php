
    <footer class="text-center p-1 bg-primary text-white fixed-bottom">
        <sup>©</sup>{{ now()->format('Y') }} Copyright : 
        <a class="text-white" href="https://hermawansafrin.com" target="_blank">
            Hermawan Safrin (KPB)
        </a>
    </footer>

    </body>
</html>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

@stack('footer_scripts')