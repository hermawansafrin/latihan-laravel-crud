<!-- Penanda template yang digunakan adalah dari app.blade.php -->
@extends('app._layouts.app')

@section('content')

<div class="card m-4">
    <div class="card-header bg-primary text-white">
        Komunitas Programmer Buton
    </div>
    <div class="card-body">
        <h5 class="card-title">Welcome !</h5>
        <p class="card-text">Ini adalah aplikasi pencatatan data siswa sederhana.</p>
        <a href="{{ route('siswa.index') }}" class="btn btn-primary btn-sm">
            Mulai <span class="fa fa-arrow-right"></span>
        </a>
    </div>
</div>

@endsection