<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    use HasFactory;

    /**
     * Artinya, kolom id di proteksi, sehingga tidak bisa di nasukkan secara masal
     */
    protected $guarded = ['id'];

    /** Mendefinisikan kolom yang boleh di isi, ini untuk keperluan keamanan data */
    protected $fillable = ['nama'];
}
