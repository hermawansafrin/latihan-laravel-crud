<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\SiswaRepository;
use App\Repositories\ProdiRepository;

use App\Http\Requests\SiswaCreateRequest;
use App\Http\Requests\SiswaEditRequest;
use App\Http\Requests\SiswaUpdateRequest;
use App\Http\Requests\SiswaDeleteRequest;

class SiswaController extends Controller
{
    /**
     * Pendefinisiaan class repository yang akan digunakan
     * @var SiswaRepository
     */
    private SiswaRepository $repo;

    /**
     * Pendifinisian class repository yang akan digunakan
     */
    private ProdiRepository $prodiRepo;

    /**
     * Melakukan init saat memanggil class siwa controller
     * @void
     */
    public function __construct()
    {
        $this->repo = app(SiswaRepository::class);
        $this->prodiRepo = app(ProdiRepository::class);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $datas  = $this->repo->get();

        return view('app.siswa.index', [
            'datas' => $datas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $prodis = $this->prodiRepo->get();

        return view('app.siswa.create', [
            'prodis' => $prodis
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param SiswaCreateRequest $request
     * 
     */
    public function store(SiswaCreateRequest $request)
    {
        /** Ambil inputan dari form */
        $input = $request->all();
        /** Lakukan action pembuatan data lewat class repository */
        $data = $this->repo->create($input);

        /** Redirect success ke store */
        return redirect()->route('siswa.index')->with('status', "Berhasil menambahkan data");

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * 
     * @param SiswaEditRequest $request
     * @param int $id
     */
    public function edit(SiswaEditRequest $request, int $id)
    {
        $data = $this->repo->findOne($id);
        $prodis = $this->prodiRepo->get();

        return view('app.siswa.edit', [
            'data' => $data,
            'prodis' => $prodis
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param SiswaUpdateRequest $request
     * @param int $id
     */
    public function update(SiswaUpdateRequest $request, int $id)
    {
        /** Ambil inputan dari form */
        $input = $request->all();
        /** Lakukan action update data */
        $data = $this->repo->update($id, $input);

        /** Redirect success ke update */
        return redirect()->route('siswa.index')->with('status', "Berhasil mengupdate data");
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param SiswaDeleteRequest $request
     * @param int $id
     * 
     */
    public function destroy(SiswaDeleteRequest $request, int $id)
    {
        $this->repo->destroy($id);
        return redirect()->route('siswa.index')->with('status', "Berhasil menghapus data");
    }
}
