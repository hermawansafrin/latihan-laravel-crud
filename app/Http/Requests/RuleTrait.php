<?php

namespace App\Http\Requests;

use App\Models\Prodi;
use App\Models\Siswa;

/**
 * Trait untuk kebutuhan validasi agar bisa reusable
 */
trait RuleTrait
{
    /**
     * Validasi kebutuhan id yang valid
     */
    public function getIdRules(): array
    {
        return [
            'bail',
            'required',
            'integer',
            'exists:'.Siswa::class.',id'  
        ];
    }

    /**
     * Validasi untuk kebutuhan nama siswa
     * @return array
     */
    public function getSiswaNamaRules(): array
    {
        return [
            'required',
            'string',
            'min:2',
            'max:50'
        ];
    }

    /**
     * Validasi untuk kebutuhan nomor induk
     * @param bool|null $onUpdate
     * @return array
     */
    public function getNomorIndukRules(?bool $onUpdate = false): array
    {
        if($onUpdate){
            return [
                'required',
                /** Agar unique nya, dikecualikan unique sedikit */
                'unique:'.Siswa::class.',nomor_induk,'.$this->id,
                'min:10',
                'max:20'
            ];
        }

        return [
            'required',
            'unique:'.Siswa::class.',nomor_induk',
            'min:10',
            'max:20'
        ];
    }

    /**
     * Validasi id prodi yang dipilih
     * @return array
     */
    public function getProdiIdRules(): array
    {
        return [
            'required',
            'integer',
            'exists:'.Prodi::class.',id'
        ];
    }

    /**
     * Validasi image yang di masukkan
     * @param bool|null $onUpdate
     * @return array
     */
    public function getImageRules(?bool $onUpdate = false): array
    {
        if($onUpdate){
            return [
                'file',
                'image',
                'max:2048',
                'mimes:png,jpg,jpeg'
            ];
        }

        return [
            'required',
            'file',
            'image',
            'max:2048',
            'mimes:png,jpg,jpeg'
        ];
    }
}