<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiswaCreateRequest extends FormRequest
{
    /** Trait untuk memanggil kebutuhan validasi */
    use RuleTrait;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'nama' => $this->getSiswaNamaRules(),
            'nomor_induk' => $this->getNomorIndukRules(),
            'prodi_id' => $this->getProdiIdRules(),
            'image' => $this->getImageRules()
        ];
    }

    /**
     * Mengatur data yang masuk pasca validasi
     * @return void
     */
    public function passedValidation(): void
    {
        $data = $this->validated();

        /** prodi_id nya dipastikan jadi integer dulu */
        $data['prodi_id'] = (int) $data['prodi_id'];

        /** Atur agar kembali ke request */
        $this->replace($data);
    }
}
