<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiswaEditRequest extends FormRequest
{
    use RuleTrait;

    /**
     * Fungsi sebelum memulai validasi
     * @return void
     */
    public function prepareForValidation(): void
    {
        $this->merge([
            'id' => (int) $this->route('id') ?? ''
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'id' => $this->getIdRules()
        ];
    }

    /**
     * Mengatur data yang masuk pasca validasi
     * @return void
     */
    public function passedValidation(): void
    {
        $data = $this->validated();

        /** prodi_id nya dipastikan jadi integer dulu */
        $data['id'] = (int) $data['id'];

        /** Atur agar kembali ke request */
        $this->replace($data);
    }
}
