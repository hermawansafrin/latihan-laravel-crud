<?php

namespace App\Repositories;

use App\Models\Prodi;

class ProdiRepository
{
    /**
     * Mengambil seluruh data prodi
     * @return array
     */
    public function get(): array
    {
        $datas = Prodi::get()->toArray();

        return $datas;
    }
}