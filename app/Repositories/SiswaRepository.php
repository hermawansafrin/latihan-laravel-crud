<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

use File;

use App\Models\Siswa;

class SiswaRepository
{
    /**
     * Mengambil data siswa beserta relasinya
     * 
     * @return Collection
     */
    public function get(): array
    {
        /** Mengambil data siswa */
        $datas = Siswa::with(['prodi:id,nama'])//ditambahkan relasi prodi
            ->get()
            ->toArray()
        ;

        return $datas;
    }

    /**
     * Melakukan proses pembuatan data
     * @param array $input
     * @return mixed
     */
    public function create(array $input): mixed
    {
        DB::beginTransaction();
        try{
            $data = new Siswa;
            /** Simpan data nama */
            $data->nama = $input['nama'];
            /** Simpan nomor induk */
            $data->nomor_induk = $input['nomor_induk'];
            /** Simpan Data Prodi */
            $data->prodi_id = $input['prodi_id'];

            /** Input Gambar */
            if(isset($input['image'])){
                /** Ambil object nya */
                $objImage = $input['image'];
                /** Simpan, lalu kemudian mendapatkan nama file hasil disimpan */
                $imageName = $this->saveImage($objImage);
                $data->image = $imageName;//save nama file nya
            }

            /** Simpan datanya */
            $data->save();
            DB::commit();

            $data->refresh();
            return $data->toArray();
        }catch(\Exception $e){
            DB::rollback();
            return null;
        }
        
    }

    /**
     * Mengambil satu data siswa berdasarkan id
     * 
     * @param int $id
     * @return siswa
     */
    public function findOne(int $id): array
    {
        $data = Siswa::with('prodi:id,nama')->find($id)->toArray() ?? [];

        return $data;
    }

    /**
     * Melakukan proses update sebuah data berdasarkan id
     * 
     * @param int $id
     * @param array $input
     * 
     * @return mixed
     */
    public function update(int $id, array $input): mixed
    {
        DB::beginTransaction();
        try{
            $data = Siswa::find($id);

            /** Jika di inputan ada image, maka perlu hapus image, lalu perbaharui */
            if(isset($input['image'])){
                /** Ambil object nya */
                $objImage = $input['image'];
                /** Delete dulu data nya */
                $this->deleteImageByName($data->image);
                /** Simpan image, lalu ambil string name file nya */
                $imageName = $this->saveImage($objImage);
                $data->image = $imageName;
            }

            $data->nama = $input['nama'];
            $data->prodi_id = $input['prodi_id'];
            $data->nomor_induk = $input['nomor_induk'];
            $data->save();

            DB::commit();
            $data->findOne($id);
        }catch(\Exception $e){
            DB::rollback();
            return null;
        }
    }

    /**
     * Melakukan proses penghapusan data
     * @param int $id
     * @return mixed
     */
    public function destroy(int $id): mixed
    {
        DB::beginTransaction();
        try{
            $data = Siswa::find($id);

            /** Jika ada image nya, dihapus dulu di storage */
            if(isset($data->image)){
                $this->deleteImageByName($data->image);
            }

            $data->delete();
            DB::commit();
            return true;
        }catch(\Exception $e){
            DB::rollback();
            return null;
        }
    }

    /**
     * Fungsi untuk menyimpan gambar dan merturnkan nama file nya
     * @param mixed $objImage
     * @return string
     */
    private function saveImage(mixed $objImage): string
    {
        $path = 'public/images/';
        /** Format nama nya */
        $image = 'image-'.time().'-'.$objImage->getClientOriginalName();
        /** Save file nya di folder path dengan nama file image */
        $objImage->storeAs($path, $image);

        return $image;
    }

    /**
     * Melakukan penghapusan sebuah file
     * @param string $image
     * @return void
     */
    private function deleteImageByName(string $image): void
    {
        $imageWithPathName = 'storage/images/'.$image;
        File::delete($imageWithPathName);
    }
}