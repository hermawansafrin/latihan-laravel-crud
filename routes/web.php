<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\IndexController;
use App\Http\Controllers\SiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/**
 * Index page nya coba di seederhanakan dengan Index Controller
 */
Route::get('/', [IndexController::class, 'index'])->name('index');

/**
 * Dibuatkan grouping agar
 */
Route::group(['prefix' => 'siswa', 'as' => 'siswa.'], function(){
    /** Route mendapatkan list data */
    Route::get('', [SiswaController::class, 'index'])->name('index');
    /** Route memunculkan form saat create */
    Route::get('/create', [SiswaController::class, 'create'])->name('create');
    /** Route unutk mengeksekusi perintah menyimpan data yang telah diinputkan */
    Route::post('store', [SiswaController::class, 'store'])->name('store');
    /** Route untuk mendapatkan detail dari sebuha data */
    Route::get('{id}', [SiswaController::class, 'show'])->name('show');
    /** Route untuk melakukan proses pengeditan (memunculkan form edit) */
    Route::get('/{id}/edit', [SiswaController::class, 'edit'])->name('edit');
    /** Route update untuk melakukan eksekusi perintah update */
    Route::put('/{id}/update', [SiswaController::class, 'update'])->name('update');
    /** Route untuk melakukan eksekusi delete */
    Route::delete('{id}', [SiswaController::class, 'destroy'])->name('destroy');

    /**
     * NOTE :
     * semua penulisan di atas, dapat diganti dengan
     * Route::resource('', [SiswaController]);
     * 
     * Akan tetapi, kembali ke preferensi masing-masing.
     * Saya pribadi lebih suka mendefinisikan nya secara jelas
     */
});
