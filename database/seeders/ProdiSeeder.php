<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Prodi;

/**
 * Seeder untuk pemenuhan kebutuhan data prodi
 */
class ProdiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /**
         * Penggunaan transaction ini digunakan untuk menjaga ke konsistensi-an data.
         * Sehingga misalkan ditengah perjalanan terdapat error, maka kondisi akan dikembalikan
         * seperti sesaat sebelum jalan nya eksekusi database
         */
        DB::beginTransaction();
        try{
            /** Fungsi pengisian data prodi */
            self::doSeedingData();
            /** Commit, sebagai tanda bahwa jika sudah benar-benar final dan selesai, maka nilai yang ada di database terkini dikuncu */
            DB::commit();
        }catch(\Exception $e){
            /** Jika terjadi error, maka ini dilakukan proses pengembalian data */
            DB::rollback();
            /** Ini sebatas penembalian data agar ketahuan apa yang sebenarnya terjadi */
            $this->command->warn("Failed");
            $this->command->error($e);
        }
    }

    /**
     * Fungsi melakukan proses pengisian data prodi.
     * Ini dipisahkan, agar bisa digunakan di lain tempat
     * @return void
     */
    private static function doSeedingData(): void
    {
        /** Melakukan list data, prodi apa ya */
        $initialDatas = [
            ['nama' => 'Teknik Informatika'],
            ['nama' => 'Teknik Elektro'],
            ['nama' => 'Sistem Informasi']
        ];

        foreach($initialDatas as $data){
            /**
             * Pendifinisian data seperti ini dibuat agar data yang di insert ke dalam data prodi,
             * Jelas masukan datanya, sehingga tidak perlu lagi melakukan dump (jika insert menggunakan skema array).
             * Semua cara benar, hanya saja tergantung preferensi masing-masing
             */
            $prodi = new Prodi;
            $prodi->nama = $data['nama'];
            $prodi->save();
        }
    }
}
