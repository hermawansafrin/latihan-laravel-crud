<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->id();
            /** Penampung data relasi data prodi */
            $table->integer('prodi_id')->unsigned();
            /** Kolom nama siswa */
            $table->string('nama', 50);
            /** Kolom Nomor Induk */
            $table->string('nomor_induk', 20)->unique();
            /** Nama file jika ada */
            $table->string('image')->nullable();

            /** timestamp created_at & updated_at */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('siswas');
    }
};
